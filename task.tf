variable "priv_key" {
  description = "private key location"
}
resource "google_compute_network" "default" {
  name = "test-network"
}
resource "google_compute_firewall" "default" {
  name    = "terraform-tcp-allow"
  network = google_compute_network.default.name

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
}
resource "google_compute_firewall" "default1" {
  name    = "terraform-udp-allow"
  network = google_compute_network.default.name
  source_ranges = ["10.0.0.23"]
  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
    
  }
}

resource "google_compute_instance" "vm_instance" {
  name          = "node${count.index + 1}"
  machine_type  = "e2-small"
  count = 1
  boot_disk {
        initialize_params {
          image = "debian-cloud/debian-10"
          size  = 20
        }
  }
  connection {
    user = "root"
    private_key = "${file(var.priv_key)}"
    timeout = "2m"
    host = "${self.network_interface.0.access_config.0.nat_ip}"   
  }
  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list" 
  }
  provisioner "remote-exec" {
    inline = [
      "apt install nginx -y",
      "echo Juneway hostname = ${self.name} ip = ${self.network_interface.0.access_config.0.nat_ip} > /var/www/html/index.nginx-debian.html"
    ]
  }
  network_interface {
        network = "default"
        access_config {}
  }
}