output "ip" {
  value = "${google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip}"
}
output "instance_name" {
  value = "${google_compute_instance.vm_instance.*.name}"
}

data "google_project" "project" {
}
output "project_id" {
  value = "${data.google_project.project.project_id }"
}
