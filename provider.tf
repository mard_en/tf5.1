provider "google" {
	project		= "pure-lantern-309207"
	region		= "us-central1"
	credentials = "cred.json"
	zone		= "us-central1-a"
}